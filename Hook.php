<?php
/*
 * This file is part of the Diamant Hook package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Hook;

class Hook
{
    /**
     * @var array
     */
    protected static $hooks = [];

    /**
     * Assign hook
     * @param  string   $name       The hook name
     * @param  mixed    $callable   A callable object
     * @param  int      $priority   The hook priority; 0 = high, 10 = low
     */
    public static function add($name, $callable, $priority = 10)
    {
        if (!isset(static::$hooks[$name])) {
            static::$hooks[$name] = array(array());
        }
        if (is_callable($callable)) {
            static::$hooks[$name][(int) $priority][] = $callable;
        }
    }

    /**
     * Invoke hook
     * @param  string $name The hook name
     * @param  mixed  ...   (Optional) Argument(s) for hooked functions, can specify multiple arguments
     */
    public static function apply($name)
    {
        if (!isset(static::$hooks[$name])) {
            static::$hooks[$name] = array(array());
        }
        if (!empty(static::$hooks[$name])) {
            // Sort by priority, low to high, if there's more than one priority
            if (count(static::$hooks[$name]) > 1) {
                ksort(static::$hooks[$name]);
            }
            $args = func_get_args();
            array_shift($args);
            foreach (static::$hooks[$name] as $priority) {
                if (!empty($priority)) {
                    foreach ($priority as $callable) {
                        call_user_func_array($callable, $args);
                    }
                }
            }
        }
    }

    /**
     * Get hook listeners
     *
     * Return an array of registered hooks. If `$name` is a valid
     * hook name, only the listeners attached to that hook are returned.
     * Else, all listeners are returned as an associative array whose
     * keys are hook names and whose values are arrays of listeners.
     *
     * @param  string     $name     A hook name (Optional)
     * @return array|null
     */
    public static function get($name = null)
    {
        if (!is_null($name)) {
            return isset(static::$hooks[(string) $name]) ? static::$hooks[(string) $name] : null;
        } else {
            return static::$hooks;
        }
    }

    /**
     * Clear hook listeners
     *
     * Clear all listeners for all hooks. If `$name` is
     * a valid hook name, only the listeners attached
     * to that hook will be cleared.
     *
     * @param  string   $name   A hook name (Optional)
     */
    public static function clear($name = null)
    {
        if (!is_null($name) && isset(static::$hooks[(string) $name])) {
            static::$hooks[(string) $name] = array(array());
        } else {
            foreach (static::$hooks as $key => $value) {
                static::$hooks[$key] = array(array());
            }
        }
    }
}