<?php
/*
 * This file is part of the Diamant Hook package.
 *
 * (c) Romain Gugert <romain.gugert@globalis-ms.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Diamant\Component\Hook\Tests;

use Diamant\Component\Hook\Hook;

class HookTest extends \PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        Hook::clear();
        Hook::add('foo', 'strlen');
        Hook::add('foo', 'strcmp', 10);
        Hook::add('foo', 'strcmp', 1);

        // set the mock classname in the property
        $reflProp = new \ReflectionProperty('\Diamant\Component\Hook\Hook', 'hooks');
        $reflProp->setAccessible(true);

        $this->assertEquals(
            [
                'foo' => [
                    0 => [],
                    10 => [
                        'strlen',
                        'strcmp'
                    ],
                    1 => [
                        'strcmp'
                    ]
                ]
            ],
            $reflProp->getValue()
        );
    }

    public function testGet()
    {
        Hook::clear();
        Hook::add('foo', 'strlen');
        Hook::add('foo', 'strcmp', 10);
        Hook::add('foo', 'strcmp', 1);

        $this->assertEquals(null, Hook::get('bar'));
        $this->assertEquals(
            [
                'foo' => [
                    0 => [],
                    10 => [
                        'strlen',
                        'strcmp'
                    ],
                    1 => [
                        'strcmp'
                    ]
                ]
            ],
            Hook::get()
        );

        $this->assertEquals(
            [
                0 => [],
                10 => [
                    'strlen',
                    'strcmp'
                ],
                1 => [
                    'strcmp'
                ]
            ],
            Hook::get('foo')
        );
    }


    public function testClear()
    {
        Hook::clear();
        Hook::add('foo', 'strlen');
        Hook::add('foo', 'strcmp', 10);
        Hook::add('foo', 'strcmp', 1);

        Hook::clear('foo');
        $this->assertEquals(
            [
                'foo' => [0 => [],]
            ],
            Hook::get()
        );
    }
}